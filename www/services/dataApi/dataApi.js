angular.module("chat")

.service("dataApi", ["$http", function($http){

  var hostname = "//localhost:8090/"


  function login(data, callback) {
    $http({
      url: hostname + "login.json",
      method : "POST",
      data : data
    }).then(function(response){
      callback(response.data);
    },
    function(error){
      callback(error);
    })
  }

  function signUp(data, callback){
    $http({
      url : hostname + "signup.json",
      method : "POST",
      data : data
    }).then(
      function(response){
        callback && callback(response.response);
      },
      function(error){
        callback && callback(error);
      },
    );
  }


  angular.extend(this, {
    signUp : signUp,
    login : login
  });
}])