angular.module("chat")

  .controller("signupCtrl", ["$scope", "dataApi", function ($scope, dataApi) {
    console.log("signupCtrl");
    
    $scope.formErrorString = "";
    $scope.submitFormAllowed = false;

    $scope.formParams = {
      firstName : "Lalnuntluanga",
      lastName : "Chhakchhuak",
      email : "tluanga34@gmail.com",
      phone : "9986510839",
      gender : "Female",
      password : "test1234",
      rePassword : "test1234",
      joiningDate : (new Date()).toISOString(),
      birthDay : (new Date()).toISOString()
    }

    function validateForm(){
      
    }


    function onSubmit() {
      console.log("submit");
      console.log($scope.formParams);

      $scope.formErrorString = "";

      if($scope.formParams.password !== $scope.formParams.rePassword) {
        $scope.formErrorString = "Passwords should be the same."
        return false;
      }

      dataApi.signUp($scope.formParams);
    }
    

    angular.extend($scope, {
      validateForm : validateForm,
      onSubmit : onSubmit
    })
  }])