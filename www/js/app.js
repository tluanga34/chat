var app = angular.module("chat", ['ui.router'])

  .config(["$stateProvider", "$locationProvider", "$httpProvider", function ($stateProvider, $locationProvider, $httpProvider) {

    $httpProvider.defaults.withCredentials = true;

    console.log("chat app");

    $stateProvider
    .state("login", {
      url: 'login',
      templateUrl: 'features/sessions/templates/login.html',
      controller: 'loginCtrl',
    })
    .state("signup", {
      url: 'signup',
      templateUrl: 'features/sessions/templates/signup.html',
      controller: 'signupCtrl',
    })
    .state("inbox", {
      url: 'inbox',
      templateUrl: 'features/inbox/templates/inbox.html',
      controller: 'inboxCtrl',
    })

    // $locationProvider.html5Mode(true);

  }]);