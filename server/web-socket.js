const WebSocket = require('ws');


function startServer(port) {
  const wss = new WebSocket.Server({
    port: port || 8000
  });

  var wsobjarr = [];

  wss.on('connection', function connection(ws) {

    console.log("connection");

    wsobjarr.push(ws);

    ws.on('message', function incoming(message) {
      console.log('received: %s', message);
      broadcastMessage(message);
    });

    ws.send('something');
  });

  var stdin = process.openStdin();

  stdin.addListener("data", function (message) {
    // note:  d is an object, and when converted to a string it will
    // end with a linefeed.  so we (rather crudely) account for that  
    // with toString() and then trim()
    broadcastMessage(message);

  });

  function broadcastMessage(message) {
    wsobjarr.forEach(function (ws) {
      ws.send(message.toString().trim());
    });
  }
}



exports.startServer = startServer;