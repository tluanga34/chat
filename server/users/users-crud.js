const mysql = require('mysql');
const utils = require('../tools/utils.js');


function getConnection(successCb, errCb) {
  const con = mysql.createConnection({
    host: "localhost",
    user: "administrator",
    password: "password",
    database: "chat"
  });

  con.connect(function (err) {
    if (err) {
      errCb(err);
      return;
    }
    successCb(con);
  });
}



//GETTING LIST OF USERS
exports.listUsers = function (successCb, failedCb) {
  getConnection(function (con) {
    var sql = `
        SELECT id, first_name, last_name, email, phone_number, gender, address_id, joining_date, birth_day FROM Users
    `;
    con.query(sql, function (err, result) {
      if (err) {
        console.log(err);
        failedCb(err);
        return;
      }
      successCb(result);
    });
  });
}


//ADDING NEW USER
exports.addUser = function (newUserObj, successCb, failedCb) {
  getConnection(function (con) {

    var {toDbStringField, toDbDateField} = utils;
    
    var sql = `
      INSERT INTO Users 
      (password, first_name, last_name, email, phone_number, address_id, gender, joining_date, birth_day) VALUES 
      (${toDbStringField(newUserObj.password)}, ${toDbStringField(newUserObj.firstName)}, ${toDbStringField(newUserObj.lastName)}, ${toDbStringField(newUserObj.email)}, ${toDbStringField(newUserObj.phone)}, null, ${toDbStringField(newUserObj.gender)}, ${toDbDateField(newUserObj.joiningDate)}, ${toDbDateField(newUserObj.birthDay)})
    `;

    console.log(sql);

    con.query(sql, function (err, result, fields) {
      if (err) {
        console.log("err.errno");
        console.log(err.errno);

        if(err.errno == 1062) {
          var errorString = `User with ${err.sqlMessage.split("'")[1]} already exist`;
          failedCb({error : errorString});
        } else {
          failedCb(err);
        }
      } else {
        successCb(result);
      }
    });
  });
}


exports.getUser = function (userObj, successCb, failedCb) {
  getConnection(function (con) {
    var sql = `
        SELECT * FROM Users where email='${userObj.userName}'
    `;
    con.query(sql, function (err, result) {
      if (err) {
        console.log("failed sql get user");
        failedCb(err);
        return;
      }

      result = result[0];

      if(!result) {
        failedCb({error: `User with ${userObj.userName} not registered`});
      }
      else if(result.password == userObj.password) {
        successCb(result);
      } else {
        failedCb({error: "Incorrect Password"});
      }

    });
  });
}