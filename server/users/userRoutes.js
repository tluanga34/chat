const userCrud = require('./users-crud');

module.exports = function (router) {

  
  
  //LOGIN USER
  router.post('/login', (req, res) => {
    userCrud.getUser(req.body, (result) => {
      sess = req.session;
      sess.userName = req.body.userName;
      res.end(JSON.stringify(result));
    }, (err) => {
      res.writeHead(401);
      res.end(JSON.stringify(err));
    });
  });




  //LOGOUT USER
  router.post('/logout', (req, res) => {
    req.session.destroy((err) => {
      if (err) {
        return console.log(err);
      }
      res.end("Logged out success");
    });
  });




  //SIGNUP USER
  router.post("/signup", (req, res) => {
    userCrud.addUser(req.body, (result) => {
      console.log("result");
      console.log(result);
      res.end(JSON.stringify(result));
    }, (err) => {
      console.log("error signup");
      res.writeHead(404);
      res.end(JSON.stringify(err));
    });
  });




  //GET USER LIST / FRIEND LIST
  router.get('/users', (req, res) => {
    console.log("Task: Get user list");
    if (!req.session.userName) {
      res.writeHead(401);
      res.end("You need to login");
      return;
    }
    userCrud.listUsers((result) => {
      res.end(JSON.stringify(result));
    }, (err) => {
      res.writeHead(401);
      res.end(JSON.stringify(err));
    });
  });


}