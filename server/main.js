const httpModule = require('./http-server.js');
const webSocketModule = require('./web-socket.js');


httpModule.startServer();
webSocketModule.startServer();