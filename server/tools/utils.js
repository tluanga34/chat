let moment = require('moment');

exports.toDbStringField = function (value) {
  return (value) ? `'${value}'` : null;
}

exports.toDbDateField = function (value) {
  return (value) ? `'${moment(value).format('YYYY-MM-DD HH:mm:ss')}'` : null;
}