const express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');
const router = express.Router();
const userRoutes = require("./users/userRoutes");


const app = express();
var sess;


app.use(session({ secret: 'ssshhhhh', saveUninitialized: true, resave: true }));
app.use(bodyParser.json());


userRoutes(router);


app.use('/', router);


exports.startServer = function (port = 8090) {
  app.listen(process.env.PORT || port, () => {
    console.log(`App Started on PORT ${process.env.PORT || port}`);
  });
}
