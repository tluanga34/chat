CREATE DATABASE chat;

USE chat;

CREATE TABLE `Address2` (
	`id` int(11) NOT NULL,
	`line_1` varchar(255) DEFAULT NULL,
	`line_2` varchar(255) DEFAULT NULL,
	`city` varchar(20) DEFAULT NULL,
	`state` varchar(20) DEFAULT NULL,
	`country` varchar(20) DEFAULT NULL,
	`landmark` varchar(30) DEFAULT NULL,
	`address_code` varchar(7) DEFAULT NULL,
	`latlng` json DEFAULT NULL,
	PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = latin1;

CREATE TABLE `Users` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`first_name` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
	`last_name` varchar(20) NOT NULL,
	`email` varchar(20) DEFAULT NULL,
	`password` varchar(25) DEFAULT NULL,
	`phone_number` varchar(20) NOT NULL,
	`address_id` int(11) DEFAULT NULL,
	`joining_date` datetime NOT NULL,
	`birth_day` datetime DEFAULT NULL,
	`gender` text CHARACTER SET latin1 COLLATE latin1_swedish_ci,
	PRIMARY KEY (`id`),
	UNIQUE KEY `phone_number` (`phone_number`),
	UNIQUE KEY `email` (`email`),
	KEY `address_id` (`address_id`),
	CONSTRAINT `Users_ibfk_1` FOREIGN KEY (`address_id`) REFERENCES `Address` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 18 DEFAULT CHARSET = latin1;